using System;
using Gdk;
using Gtk;
using Glade;

public class MainClass
{
	//MAGO SAUL VARIABLES
	int[][] Tables = new int[][]{
	new int[]{3,6,7,10,11,2,14,15,18,19,22,23,26,27,30,31,34,35,38,39,42,43,46,47,50,51,54,55,58,59},
	new int[]{3,5,7,9,11,1,13,15,17,19,21,23,25,27,29,31,33,35,37,39,41,49,45,47,49,51,53,55,57,59},
	new int[]{5,6,7,13,12,4,14,15,20,21,22,23,28,29,30,31,36,37,38,39,44,45,46,47,52,53,54,55,60,13},
	new int[]{33,34,35,36,37,32,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,46},
	new int[]{17,18,19,20,21,16,22,23,24,25,26,27,28,29,30,31,48,49,50,51,52,53,54,55,56,57,58,59,60,31},
	new int[]{9,10,11,12,13,8,14,15,24,25,26,27,28,29,30,31,40,41,42,43,44,45,46,47,56,57,58,59,60,13}};
	bool[] SelectedTables = new bool[6];	
	bool[,] SolutionTable;
	
	//GLADE VARIABLES
	[Glade.Widget("wnd_saul_main")] Gtk.Window wnd_saul_main;
	[Glade.WidgetAttribute] Gtk.CheckButton chkbtn_saul_t1;
	[Glade.WidgetAttribute] Gtk.CheckButton chkbtn_saul_t2;
	[Glade.WidgetAttribute] Gtk.CheckButton chkbtn_saul_t3;
	[Glade.WidgetAttribute] Gtk.CheckButton chkbtn_saul_t4;
	[Glade.WidgetAttribute] Gtk.CheckButton chkbtn_saul_t5;
	[Glade.WidgetAttribute] Gtk.CheckButton chkbtn_saul_t6;
	[Glade.WidgetAttribute] Gtk.Label lbl_saul_result;
	[Glade.WidgetAttribute] Gtk.Image img_saul_table1;
	[Glade.WidgetAttribute] Gtk.Image img_saul_table2;
	[Glade.WidgetAttribute] Gtk.Image img_saul_table3;
	[Glade.WidgetAttribute] Gtk.Image img_saul_table4;
	[Glade.WidgetAttribute] Gtk.Image img_saul_table5;
	[Glade.WidgetAttribute] Gtk.Image img_saul_table6;
	Glade.XML GUI;
	
	public MainClass()
	{
		GUI = new Glade.XML("saul1.glade","wnd_saul_main");
		GUI.Autoconnect(this);
		wnd_saul_main.Icon = new Gdk.Pixbuf(System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream("CTIcon.png"));
		img_saul_table1.Pixbuf = new Gdk.Pixbuf(System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream("Table1.png"));
		img_saul_table2.Pixbuf = new Gdk.Pixbuf(System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream("Table2.png"));
		img_saul_table3.Pixbuf = new Gdk.Pixbuf(System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream("Table3.png"));
		img_saul_table4.Pixbuf = new Gdk.Pixbuf(System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream("Table4.png"));
		img_saul_table5.Pixbuf = new Gdk.Pixbuf(System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream("Table5.png"));
		img_saul_table6.Pixbuf = new Gdk.Pixbuf(System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream("Table6.png"));
		this.Reiniciar();
		wnd_saul_main.ShowAll();
	}
	
	public void Reiniciar()
	{		
		lbl_saul_result.Text = string.Empty;
		chkbtn_saul_t1.Active = false;
		chkbtn_saul_t2.Active = false;
		chkbtn_saul_t3.Active = false;
		chkbtn_saul_t4.Active = false;
		chkbtn_saul_t5.Active = false;
		chkbtn_saul_t6.Active = false;
	}
	
	public int SearchSolutionFinal()
	{
		//Iniciar variables				
		int Number = 0;
		int NCount;

		SolutionTable = this.SearchSolutions();
		
		//Obtener casillas seleccionadas				
		SelectedTables[0] = chkbtn_saul_t1.Active;
		SelectedTables[1] = chkbtn_saul_t2.Active;	
		SelectedTables[2] = chkbtn_saul_t3.Active;
		SelectedTables[3] = chkbtn_saul_t4.Active;
		SelectedTables[4] = chkbtn_saul_t5.Active;
		SelectedTables[5] = chkbtn_saul_t6.Active;
		
		for(int i = 0; i <= 61 - 1; i++)
		{
			NCount = 0;
			for(int a = 0; a <= 6 - 1; a++)
			{
				if(SolutionTable[i,a] == SelectedTables[a])
				{
					NCount++;
				}
				
				if(NCount == 6)
				{
					Number = i;
				}
			}
		}      
		
		return Number;
	}
	
	public bool SearchNumber(int number, int[] matriz)
	{
		for (int i = 0; i <= matriz.Length - 1; i++)
		{
			if (matriz[i] == number)
			{
				return true;
			}
		}
		return false;
	}
	
	public bool[,] SearchSolutions()
	{
		bool[,] temp = new bool[61,6];
		
		//Componente con codigo STEVEN
		for (int i = 0; i <= 61 - 1; i++)
		{
			for (int a = 0; a <= 6 - 1; a++)
			{				
				temp[i,a]  = this.SearchNumber(i,Tables[a]);			
			}
		}
		return temp;
	}	
	
	public void wnd_saul_main_delete(object sender, Gtk.DeleteEventArgs args)
	{
		Gtk.Application.Quit();
	}
	
	public void btn_saul_exit_clicked(object sender, System.EventArgs args)
	{
		Gtk.Application.Quit();
	}
	
	public void btn_saul_search_clicked(object sender, System.EventArgs args)
	{
		int temp = this.SearchSolutionFinal();
		switch(temp)
		{
			case 0:
				lbl_saul_result.Text = "¿En que numero estabas pensando?";
				break;
			default:
				lbl_saul_result.Text = "El numero pensado es " + Convert.ToString(temp);
				break;
		}
		this.UpdateGTK();
		System.Threading.Thread.Sleep(1000);
		this.Reiniciar();
	}
	
	
	public void UpdateGTK()
	{
		while (Application.EventsPending())
              Application.RunIteration();
	}
	
	static void Main()
	{
		Gtk.Application.Init();
		new MainClass();
		Gtk.Application.Run();
	}
}
